import logging
import os
import time
from typing import Tuple

from playwright.sync_api import sync_playwright
from prometheus_client import start_http_server, Gauge  # type: ignore


class GitlabExporter:
    def __init__(self):
        self.monthly_minutes_used = Gauge(
            "gitlab_monthly_ci_minutes_used", "CI minutes used in our monthly quota"
        )
        self.additional_minutes_used = Gauge(
            "gitlab_additional_ci_minutes_used",
            "CI minutes used after the monthly quota has been finished",
        )
        self.monthly_quota = Gauge(
            "gitlab_monthly_quota", "The CI minutes included in our GitLab subscription"
        )
        self.additional_quota = Gauge(
            "gitlab_additional_quota", "The additional CI minutes that can be used"
        )
        self.logger = logging.getLogger("gitlab-prometheus-exporter")
        self.logger.setLevel(level=os.environ.get("GITLAB_EXPORTER_LOG_LEVEL", "WARN"))

    def get_str_from_gitlab(self) -> Tuple[str, str]:
        self.logger.info("fetching stats from gitlab")
        gitlab_sign_in_url = "https://gitlab.com/users/sign_in"
        gitlab_quota_url = (
            "https://gitlab.com/groups/TankerHQ/-/usage_quotas#pipelines-quota-tab"
        )
        with sync_playwright() as p:
            browser = p.firefox.launch(headless=True)
            page = browser.new_page()
            page.goto(gitlab_sign_in_url, wait_until="networkidle")
            page.fill("id=user_login", "sysadmin@tanker.io")
            page.fill("id=user_password", os.environ["GITLAB_PASSWORD"])
            page.click("text=Sign in")
            page.wait_for_selector("text=Projects")
            page.goto(gitlab_quota_url, wait_until="networkidle")
            # First element is the monthly usage
            monthly = page.inner_text(".shared_runners_limit_under_quota")
            # Second element is the additional minutes
            try:
                additional = page.inner_text(
                    ":nth-match(.shared_runners_limit_under_quota, 2)"
                )
            # The additional minutes are only displayed if we have some.
            # Probably playwright._impl._api_types.TimeoutError.
            # playwright does not expose cleanly its exceptions yet.
            except Exception as e:
                self.logger.debug(e)
                additional = "0 / 0"
            browser.close()
        return monthly, additional

    def get_metrics(self) -> None:
        monthly, additional = self.get_str_from_gitlab()
        self.logger.info("fetched %s, %s from gitlab", monthly, additional)
        self.monthly_minutes_used.set(int(monthly.split(" / ")[0]))
        self.monthly_quota.set(int(monthly.split(" / ")[1]))
        self.additional_minutes_used.set(int(additional.split(" / ")[0]))
        self.additional_quota.set(int(additional.split(" / ")[1]))


def main() -> None:
    logging.basicConfig()
    exporter = GitlabExporter()
    port = int(os.environ["GITLAB_EXPORTER_PORT"])
    interval = int(os.environ["GITLAB_EXPORTER_INTERVAL"])
    start_http_server(port)
    exporter.logger.info("listening on port %s", port)
    while True:
        try:
            exporter.get_metrics()
            exporter.logger.info("sleeping for %s seconds", interval)
            time.sleep(interval)
        except KeyboardInterrupt:
            break
