FROM python:3.9.4-buster

ENV DEBIAN_FRONTEND=interactive

RUN apt update && apt install -y firefox-esr
RUN pip install --upgrade pip
RUN pip install poetry

COPY poetry.lock ./poetry.lock
COPY pyproject.toml ./pyproject.toml
COPY gitlab_prometheus_exporter/__init__.py ./gitlab_prometheus_exporter/__init__.py
COPY gitlab_prometheus_exporter/server.py ./gitlab_prometheus_exporter/server.py
RUN poetry install --no-dev
RUN poetry run playwright install

ENTRYPOINT ["poetry", "run", "gitlab-prometheus-exporter"]
